Given("I'm at the index page") do
  visit(ELEMENTS["url"])
end

Given("I'm logged") do
    find("#sign_in").click()
    within(".panel-body") do
      find(ELEMENTS['txt_email']).fill_in(with:"pedro.lockley@hotmail.com")
      find(ELEMENTS['txt_pass']).fill_in(with: "Senhadoteste")
      find(ELEMENTS['btn_login']).click()
    end
end

When("I click the My Tasks link") do
  find(ELEMENTS['lnk_myTasks']).click()
end

Then("I should be redirected to the My tasks page") do
  assert_selector(ELEMENTS['txt_taskDesc'])
end

When("I go to the My tasks page") do
  find("#sign_in").click()
  within(".panel-body") do
    find(ELEMENTS['txt_email']).fill_in(with:"pedro.lockley@hotmail.com")
    find(ELEMENTS['txt_pass']).fill_in(with: "Senhadoteste")
    find(ELEMENTS['btn_login']).click()
  end
  find(ELEMENTS['lnk_myTasks']).click()
end

When("I see the message") do
  assert_text("Pedro Henrique Lockley's ToDo List")
end

Then("I should have my username on the message") do
  assert_text("Pedro Henrique Lockley", count:2)
end

Given("I'm at the My tasks page") do
  visit(ELEMENTS['url'])
  find("#sign_in").click()
  within(".panel-body") do
    find(ELEMENTS['txt_email']).fill_in(with:"pedro.lockley@hotmail.com")
    find(ELEMENTS['txt_pass']).fill_in(with: "Senhadoteste")
    find(ELEMENTS['btn_login']).click()
  end
  find(ELEMENTS['lnk_myTasks']).click()
end

Given("I fill the description field") do
    find(ELEMENTS['txt_taskDesc']).fill_in(with: ELEMENTS['taskText'])
end

Given("I fill in the necessary fields") do
    find(ELEMENTS['txt_subtaskDesc']).fill_in(with: ELEMENTS['subtaskText'])
    find(ELEMENTS['txt_dueDate']).fill_in(with: ELEMENTS['duedateText'])
end

When("I click the Add Task button") do
  find(ELEMENTS['btn_addTask']).click()
end

Then("I see a new task appended to the list") do
  assert_selector(ELEMENTS['taskItem'])
end

Given("the task description field has less than {int} characters") do |int|
  if(find(ELEMENTS['txt_taskDesc']).value.length > 3)
      return true
  end
end

When("I press Enter") do
  find(ELEMENTS['txt_taskDesc']).send_keys[:enter]
end

Then("I expect an error message") do
  assert_text("error")
end

Given("type {int} characters on the task description field") do |int|
  find(ELEMENTS['txt_taskDesc']).fill_in(with: ELEMENTS['str_max'])
end

When("I type one more character") do
  find(ELEMENTS['txt_taskDesc']).send_keys("s")
end

Then("I expect the text not to change") do
  raise "The text changed" if (ELEMENTS['txt_taskDesc'] == ELEMENTS['str_max'])
end

Given("I see the Manage Subtasks button") do
  first(ELEMENTS['btn_manSubs'])
end

When("I click the button") do
  first(ELEMENTS['btn_manSubs']).click()
end

Then("I should see the subtasks popup") do
  assert_selector(ELEMENTS['txt_subtaskDesc'])
end

Then("I see all the fields") do
  assert_selector(ELEMENTS['txt_subtaskDesc'])
  assert_selector(ELEMENTS['txt_dueDate'])
end

Given("I see the number of subtasks") do
  within('.bs-example') do
    assert_text(find(ELEMENTS['btn_manSubs']).text)
  end
end

Then("I should see the same number of subtasks on the list") do
  subtasks = all(ELEMENTS['btn_manSubs']).count()
  valores = []
  valor_formatado = []
  valores.each do |texto|
     texto.delete!("(").delete!(") Manage Subtasks")
     valor_formatado << texto.to_i
   end
   raise "The number of tasks on the button is wrong" if valor_formatado == subtasks
end

Given("I click the Manage Subtasks button") do
  first(ELEMENTS['btn_manSubs']).click
end

Given("type {int} characters on the Subtask description field") do |int|
  find(ELEMENTS['txt_subtaskDesc']).fill_in(with: ELEMENTS["str_max"])
end

When("I click the Add subtask button") do
  find(ELEMENTS['btn_addsubtask']).click
end

Then("I see a new subtask appended to the list") do
  within(all(ELEMENTS['subtaskList'])[1]) do
    assert_selector(ELEMENTS['subtaskItem'])
  end
end

Given("I don't fill in the necessary fields") do
  find(ELEMENTS['txt_subtaskDesc']).set("")
  find(ELEMENTS['txt_dueDate']).set("")
end
