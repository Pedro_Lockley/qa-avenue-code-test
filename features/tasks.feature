#language: en

Feature: Task manager, the user should be able to create and keep track of their
tasks and subtasks.

 @first
  Scenario: The user should see a "My tasks" link on the navbar and when clicked
  should redirect them to the "My tasks" page
    Given I'm at the index page
    And I'm logged
    When I click the My Tasks link
    Then I should be redirected to the My tasks page

 @second
  Scenario: Verify if the message at the My tasks page shows the correct user
  name
    Given I'm at the index page
    When I go to the My tasks page
    And I see the message
    Then I should have my username on the message

 @third
  Scenario: Add a new task
    Given I'm at the My tasks page
    And I fill the description field
    When I click the Add Task button
    Then I see a new task appended to the list

@forth
  Scenario: Test the character threshold to add a new task using enter
    Given I'm at the My tasks page
    And the task description field has less than 3 characters
    When I press Enter
    Then I expect an error message

@fifth
  Scenario: Test the character maximum on the task description text box
    Given I'm at the My tasks page
    And type 250 characters on the task description field
    When I type one more character
    Then I expect the text not to change

@sixth
  Scenario: The user should see a "Manage Subtasks" button on the list and when
  clicked should open the subtasks popup with a subtask description field and a
  due date field
    Given I'm at the My tasks page
    And I see the Manage Subtasks button
    When I click the button
    Then I should see the subtasks popup
    And I see all the fields

@seventh
  Scenario: The user should see the number of subtasks of that task on the
  task's button
    Given I'm at the My tasks page
    And I see the Manage Subtasks button
    And I see the number of subtasks
    When I click the button
    Then I should see the same number of subtasks on the list

@eighth
  Scenario: Test the character maximum on the Subtask description text box
    Given I'm at the My tasks page
    And I click the Manage Subtasks button
    And type 250 characters on the Subtask description field
    When I type one more character
    Then I expect the text not to change

@ninth
  Scenario: Add a new subtask
    Given I'm at the My tasks page
    And I click the Manage Subtasks button
    And I fill in the necessary fields
    When I click the Add subtask button
    Then I see a new subtask appended to the list

@tenth
  Scenario: Test if the subtask fields are being required
    Given I'm at the My tasks page
    And I click the Manage Subtasks button
    And I don't fill in the necessary fields
    When I click the Add subtask button
    Then I expect an error message
